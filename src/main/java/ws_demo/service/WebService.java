package ws_demo.service;

import javax.jws.WebMethod;

@javax.jws.WebService
public interface WebService {
    @WebMethod
    String insert(Object obj);
}
